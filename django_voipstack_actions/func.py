from django_voipstack_actions.django_voipstack_actions.models import *


def add_job_option(job, option_name, value):
    new_opt = VoipJobOptions(
        job=job, attribute=VoipJobAttributes.objects.get(name=option_name), value=value
    )

    new_opt.save()

    return True
