from django.apps import apps
from django.contrib import admin

from . import models

for model in apps.get_app_config("django_voipstack_actions").models.values():
    if model == models.VoipJob:
        continue

    admin.site.register(model)


# Jobs
class VoipJobOptionsInLine(admin.TabularInline):
    model = models.VoipJobOptions
    extra = 0


class VoipJobLogInLine(admin.TabularInline):
    model = models.VoipJobLog
    extra = 0
    readonly_fields = [f.name for f in model._meta.fields]
    can_delete = False


@admin.register(models.VoipJob)
class VoipJobAdmin(admin.ModelAdmin):
    inlines = [VoipJobOptionsInLine, VoipJobLogInLine]
