# Generated by Django 3.0.5 on 2020-04-14 14:55

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_voipstack_mdat", "0019_use_cascade_for_models"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("django_voipstack_actions", "0020_add_form_order_field"),
    ]

    operations = [
        migrations.AlterField(
            model_name="voipactionsmandatoryattrs",
            name="voipactions",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to="django_voipstack_actions.VoipActions",
            ),
        ),
        migrations.AlterField(
            model_name="voipactionsmandatoryattrs",
            name="voipjobattributes",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to="django_voipstack_actions.VoipJobAttributes",
            ),
        ),
        migrations.AlterField(
            model_name="voipjob",
            name="action",
            field=models.ForeignKey(
                db_column="action",
                on_delete=django.db.models.deletion.CASCADE,
                to="django_voipstack_actions.VoipActions",
            ),
        ),
        migrations.AlterField(
            model_name="voipjob",
            name="job_state",
            field=models.ForeignKey(
                db_column="job_state",
                on_delete=django.db.models.deletion.CASCADE,
                to="django_voipstack_actions.VoipJobCarrierStates",
            ),
        ),
        migrations.AlterField(
            model_name="voipjob",
            name="user",
            field=models.ForeignKey(
                db_column="user",
                on_delete=django.db.models.deletion.CASCADE,
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AlterField(
            model_name="voipjobattributevalues",
            name="voipjobattribute",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to="django_voipstack_actions.VoipJobAttributes",
            ),
        ),
        migrations.AlterField(
            model_name="voipjobcarrierstates",
            name="carrier",
            field=models.ForeignKey(
                db_column="carrier",
                on_delete=django.db.models.deletion.CASCADE,
                to="django_voipstack_mdat.VoipMdatCarrier",
            ),
        ),
        migrations.AlterField(
            model_name="voipjoblog",
            name="carrier",
            field=models.ForeignKey(
                db_column="carrier",
                on_delete=django.db.models.deletion.CASCADE,
                to="django_voipstack_mdat.VoipMdatCarrier",
            ),
        ),
        migrations.AlterField(
            model_name="voipjoblog",
            name="job",
            field=models.ForeignKey(
                db_column="job",
                on_delete=django.db.models.deletion.CASCADE,
                to="django_voipstack_actions.VoipJob",
            ),
        ),
        migrations.AlterField(
            model_name="voipjoboptions",
            name="attribute",
            field=models.ForeignKey(
                db_column="attribute",
                on_delete=django.db.models.deletion.CASCADE,
                to="django_voipstack_actions.VoipJobAttributes",
            ),
        ),
        migrations.AlterField(
            model_name="voipjoboptions",
            name="job",
            field=models.ForeignKey(
                db_column="job",
                on_delete=django.db.models.deletion.CASCADE,
                to="django_voipstack_actions.VoipJob",
            ),
        ),
    ]
