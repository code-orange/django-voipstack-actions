from django.conf import settings

from django_voipstack_mdat.django_voipstack_mdat.models import *


class VoipJobAttributes(models.Model):
    name = models.CharField(unique=True, max_length=50)
    html_input_type = models.CharField(
        max_length=30, default="text", null=True, blank=True
    )
    default_value = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "voip_job_attributes"


class VoipActions(models.Model):
    ENABLED = 1
    DISABLED = 0

    ENABLED_CHOICES = (
        (ENABLED, "ENABLED"),
        (DISABLED, "DISABLED"),
    )

    ACTION_FIRST = 0
    ACTION_FOLLOW = 1

    ACTION_CHOICES = (
        (ACTION_FIRST, "ACTION_FIRST"),
        (ACTION_FOLLOW, "ACTION_FOLLOW"),
    )

    name = models.CharField(max_length=50, unique=True)
    follow_task = models.IntegerField(choices=ACTION_CHOICES, default=ACTION_FIRST)
    enabled = models.IntegerField(choices=ENABLED_CHOICES, default=ENABLED)
    mandatory_attrs = models.ManyToManyField(
        VoipJobAttributes, through="VoipActionsMandatoryAttrs"
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "voip_actions"


class VoipActionsMandatoryAttrs(models.Model):
    voipactions = models.ForeignKey(VoipActions, models.CASCADE)
    voipjobattributes = models.ForeignKey(VoipJobAttributes, models.CASCADE)
    html_input_type = models.CharField(max_length=30, null=True, blank=True)
    default_value = models.CharField(max_length=100, null=True, blank=True)
    form_order = models.IntegerField(default=0)

    class Meta:
        db_table = "voip_actions_mandatory_attrs"
        unique_together = (("voipactions", "voipjobattributes"),)


class VoipJobAttributeValues(models.Model):
    voipjobattribute = models.ForeignKey(VoipJobAttributes, models.CASCADE)
    name = models.CharField(max_length=200, null=True, blank=True)
    value = models.CharField(max_length=200, null=True, blank=True)

    class Meta:
        db_table = "voip_job_attribute_values"
        unique_together = (("voipjobattribute", "value"),)


class VoipJob(models.Model):
    id = models.BigAutoField(primary_key=True)
    action = models.ForeignKey(VoipActions, models.CASCADE, db_column="action")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE, db_column="user")
    job_state = models.ForeignKey(
        "VoipJobCarrierStates", models.CASCADE, db_column="job_state"
    )
    desire_date = models.DateField(default=datetime.today)
    date_added = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        if self.pk is None:
            now = datetime.now()
            date_code = now.strftime("%Y%m%d")

            try:
                latest_pk = VoipJob.objects.filter(pk__startswith=date_code).latest(
                    "pk"
                )
                new_pk = int(latest_pk.pk) + 1
            except VoipJob.DoesNotExist:
                new_pk = int(date_code + "000001")

            self.pk = new_pk
        super(VoipJob, self).save(*args, **kwargs)

    class Meta:
        db_table = "voip_job"


class VoipJobOptions(models.Model):
    id = models.BigAutoField(primary_key=True)
    job = models.ForeignKey(VoipJob, models.CASCADE, db_column="job")
    attribute = models.ForeignKey(
        VoipJobAttributes, models.CASCADE, db_column="attribute"
    )
    value = models.TextField(blank=True)

    class Meta:
        db_table = "voip_job_options"
        unique_together = (("job", "attribute"),)


class VoipJobLog(models.Model):
    id = models.BigAutoField(primary_key=True)
    job = models.ForeignKey(VoipJob, models.CASCADE, db_column="job")
    log_row = models.PositiveIntegerField(default=0)
    carrier = models.ForeignKey(
        "django_voipstack_mdat.VoipMdatCarrier", models.CASCADE, db_column="carrier"
    )
    log_text = models.TextField()
    status_code = models.IntegerField(default=200)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "voip_job_log"
        unique_together = (("job", "log_row"),)


class VoipJobCarrierStates(models.Model):
    name = models.CharField(max_length=100)
    carrier = models.ForeignKey(
        "django_voipstack_mdat.VoipMdatCarrier", models.CASCADE, db_column="carrier"
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "voip_job_carrier_states"
        unique_together = (("name", "carrier"),)
